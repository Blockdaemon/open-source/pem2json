#!/usr/bin/env python3

import os, sys
import tempfile
import json
import jq

import argparse
import pem
import ssl

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('pemfile', nargs='?', type=argparse.FileType('rb'), default=sys.stdin.buffer)
    group_strip = parser.add_mutually_exclusive_group()
    group_strip.add_argument('--strip-last-cert', help='strip last cert', action='store_true')
    group_json = parser.add_mutually_exclusive_group()
    group_json.add_argument('--cn', help='show CN (subject.commonNames)', action='store_true')
    group_json.add_argument('--exp', help='show expiration (notAfter)', action='store_true')
    args = parser.parse_args()
    script = []

    try:
        pems = pem.parse(args.pemfile.read())
    except Exception as e:
        print(f"Error decoding pem: {e}\n")
        sys.exit(1)

    if args.strip_last_cert:
        if len(pems)>1:
            for p in pems[:-1]:
                print(p)
        else:
            for p in pems:
                print(p)
        sys.exit(0)

    if args.cn:
        script.append('"subject": {"commonName": .subject.commonName}')

    if args.exp:
        script.append('"notAfter": .notAfter')

    script = f"{{ {','.join(script)} }}" if len(script)>0 else None

    print(output(pems, script))

def decode(pem):
    f = tempfile.NamedTemporaryFile(mode='w', delete=False)
    f.write(pem.as_text())
    f.close()

    try:
        cert = ssl._ssl._test_decode_cert(f.name)
        os.unlink(f.name)
    except Exception as e:
        os.unlink(f.name)
        print(f"Error decoding certificate: {e}\n")
        return

    # remap array mess in subject and issuer into dicts
    for item in ['subject', 'issuer']:
        if item in cert:
            item_dict = {}
            for s in cert[item]:
                for i in s:
                    if i[0] not in item_dict:
                        item_dict[i[0]] = []
                    item_dict[i[0]].append(i[1])
            # collapse single item arrays into scalars
            for k,v in item_dict.items():
                if len(v) == 1:
                    item_dict[k] = v[0]
            cert[item] = item_dict

    # remap array mess in subjectAltName into a dict
    if 'subjectAltName' in cert:
        item = 'subjectAltName'
        san_dict = {}
        for i in cert[item]:
            if i[0] not in san_dict:
                san_dict[i[0]] = []
            san_dict[i[0]].append(i[1])
        cert[item] = san_dict

    return cert

def output(pems, script=None):
    res = []
    for p in pems:
        cert = decode(p)
        if script:
            cert = jq.first(script, cert)
        if cert:
            res.append(cert)

    return json.dumps(res, indent=2)

if __name__ == "__main__":
    main()

# vim: sw=4:expandtab
